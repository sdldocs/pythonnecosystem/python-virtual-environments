# Python Virtual Environments 기초

Python으로 매플리케이션을 개발할 때 개발환경 구축중 중요한 사항 중 하나인 **Python Virtual Environments**에 대한 간단한 입문서이다.

이는 [Python Virtual Environments: A Primer](https://realpython.com/python-virtual-environments-a-primer/)를 슈어데이터랩 환경에 적용할 수 있도록 편역한 것이다.

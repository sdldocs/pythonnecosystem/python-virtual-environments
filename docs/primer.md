# Python Virtual Environments 기초

## Virtual Environments가 필요한 이유? <a id="why-the-need-for-virtual-environments"></a>

Python은 대부분의 현대 프로그래밍 언어처럼 패키지(또는 [모듈](https://en.wikipedia.org/wiki/Modular_programming))를 다운로드하고, 저장하며 그리고 이를 지정하는 고유한 방법을 가지고 있다. 이 방법은 장점이 있지만, 패키지 스토리지와 지정에 있어서 *흥미로운* 점(?)이 있다. 이로 인해 특히 패키지를 저장하는 방법과 장소에 문제가 발생할 수 있다.

시스템에 이러한 패키지를 다른 위치에 설치할 수 있다. 예를 들어 대부분의 시스템 패키지는 [sys.prefix](https://docs.python.org/3/library/sys.html#sys.prefix)에 저장된 경로의 하위 디렉터리에 저장된다.

Ubuntu에서는 Python 쉘을 사용하여 `sys.prefix`이 지정하는 위치를 쉽게 찾을 수 있다.

```python
>>> import sys
>>> sys.prefix
'/usr/local'
```

여기에서 다루고자 하는 문제와 보다 관련성이 높은 것은 [easy_install](https://pythonhosted.org/setuptools/easy_install.html) 또는 [pip](https://en.wikipedia.org/wiki/Pip_(package_manager))을 사용하여 설치된 서드파티 패키지는 통상적으로 [site.getsitepackages](https://docs.python.org/3/library/site.html#site.getsitepackages)에 지정된 디렉토리 중 하나에 위치하고 있다.
```python
>>> import site
>>> site.getsitepackages()
['/usr/local/lib/python3.8/site-packages']
```

그럼, 왜 이 사소한 것들이 중요할까?

기본적으로는 시스템상의 모든 프로젝트가 이러한 디렉토리를 사용하여 **site packages**(서드파티 라이브러리)를 저장하고 검색하기 때문에 이 점을 염두에 두는 것이 중요하다. 언뜻 보기에 큰 문제가 되지 않을 수 있지만 **system packages**( Python 표준 라이브러리의 일부 패키지)에 대해서는 그렇지 않지만 **site packages**에 대해서는 문제가 된다.

2개의 프로젝트 *ProjectA*와 *ProjectB*가 있고, 모두 같은 라이브러리 *ProjectC*에 의존한다(호출한다)고 가정한다. 두 프로젝트 각각 다른 버전의 *ProjectC*가 필요할 때 문제가 명백해진다. 예를 들어 *ProjectA*는 *ProjectC* v1.0.0이 필요하고 *ProjectB*는 새로운 v2.0.0이 필요할 수 있다.

이 경우 Python에 있어서 진짜 문제가 된다. 왜냐하면 Python은 `site-packages` 디렉토리에서 버전을 구분하지 않기 때문이다. 따라서 v1.0.0과 v2.0.0은 모두 같은 이름으로 디렉토리에 저장된다.
 ```
 /usr/local/lib/python3.8/ProjectC
 ```

*ProjectC*는 오직 이름만 저장되므로 버전을 구분할 수 없다. 따라서 *ProjectA*와 *ProjectB*는 모두 동일한 버전을 사용해야 하며, 대부분 경우 구별할 수 없다.

이 때문에 가상환경과 [virtualenv](https://virtualenv.readthedocs.org/en/latest/)/[venv](https://docs.python.org/3/library/venv.html)가 필요한 것이다.

## Virtual Environments 란? <a id="what-is-a-virtual-environments"></a>

Python 가상 환경의 핵심은 [Python 프로젝트](https://realpython.com/intermediate-python-project-ideas/)를 위한 격리된 환경을 만드는 것이다. 즉, 다른 프로젝트의 종속성에 관계없이 프로젝트 각각의 고유한 종속성을 갖을 수 있도록 하는 것이다.

위의 예에서 *ProjectA*와 *ProjectB*를 위해 별도의 가상 환경을 구축할 수 있다면 각 가상환경에서 어느 버전의 *ProjectC*를 선택해도, 다른 가상환경에 의존하지 않게 된다.

이 방법의 장점은 사용할 수 있는 가상환경의 수에 제한이 없다는 것이다. 왜냐하면 가상환경은 몇 개의 스크립트로 이루어진 디렉토리들 이기 때문이다. 또한 virtualenv 또는 [pyenv](https://realpython.com/intro-to-pyenv/) 명령을 사용하여 쉽게 만들 수 있다.

## Virtual Environments 사용 <a id="using-virtual-environments"></a>

Python 3을 사용하는 것으로 가정하고, Python 3 표준 라이브러리에는 이미 설치된 [venv](https://docs.python.org/3/library/venv.html) 모듈이 포함되어 있다.

> **Note**: 이제부터는 새로운 venv 툴을 사용하고 있다고 가정한다.실제 명령어는 virtualenv와 거의 차이가 없기 때문이다. 그러나 실제로는 완전 [다른](https://www.reddit.com/r/learnpython/comments/4hsudz/pyvenv_vs_virtualenv/) 도구이다.

먼저 작업할 새 디렉토리를 만든다.
```shell
$ mkdir python-virtual-environments && cd python-virtual-environments
```

디렉토리내에 새로운 가상 환경을 생성한다.
```shell
# Python 3
$ python3 -m venv env
```

> **Note**: 기본적으로 기존 site-package를 **포함하지 않는다**.

Python 3 venv 접근 방법은 가상 환경을 만드는 데 사용하는 Python 3 인터프리터의 버전을 선택할 수 있는 이점이 있다. 이를 통해 새로운 환경의 기반이 되는 Python 설치에 대한 혼동을 피할 수 있다.

Python 3.3에서 3.4까지 가상 환경을 만드는 방법으로 Python 3에 기본적으로 포함된 pyvenv 명령을 사용하는 것을 권장한다. 그러나 버전 3.6 이상에서는 python3 -m venv가 더 적합하다.

위의 예에서는 `env`라는 이름의 디렉토리를 만들었다. 이 디렉토리는 다음과 같은 디렉토리 구조를 갖는다.

```
├── bin
│   ├── Activate.ps1
│   ├── activate
│   ├── activate.csh
│   ├── activate.fish
│   ├── pip
│   ├── pip3
│   ├── pip3.8
│   ├── python -> /usr/bin/python
│   └── python3 -> python
├── include
├── lib
│   └── python3.8
│       └── site-packages
└── pyvenv.cfg
```

`emv`내 생성된 폴더는 다음과 같다.

- `bin`: 가상 환경과 상호 작용하는 파일
- `include`: Python 패키지를 컴파일하는 C 헤더
- `lib`: Python 버전의 복사본과 각 종속성이 설치된 `site-packages` 폴더

또한 Python 실행 파일 자체뿐만 아니라 몇 가지 다른 Python 도구의 복사 또는 [심볼릭 링크(symlinks)](https://en.wikipedia.org/wiki/Symbolic_link)도 있다. 이러한 파일은 모든 Python 코드와 명령어가 현재 환경의 컨텍스트 내에서 실행되도록 하는 데 사용되며, 이것이 글로벌 환경으로부터 격리시키는 방법인 것이다. [다음 절](how-does-a-virtual-environments-work)에서 내용을 자세히 설명한다.

`bin` 디렉토리에 있는 **activate scripts**에 관심을 가지면 흥미로울 것이다. 이러한 스크립트는 기본적으로 해당 환경의 Python 실행 파일과 site-packages를 사용할 수 있도록 쉘을 설정하는 데 사용된다.

이 환경의 격리된 패키지 또는 리소스를 사용하려면 해당 환경을 `"activate"`해야 한다. 이를 위하여 다음 명령을 수행하여야 한다.
```shell
$ source env/bin/activate
(env) $ 
```

프롬프트 앞에 환경 이름(이 경우 `env`)이 붙는다. 이는 `env`가 현재 활성화되어 있음을 나타낸다. 즉, 파이썬 실행 파일은 이 환경의 패키지와 설정만을 사용한다.

실제 패키지 격리를 보여주기 위하여, [bcrypt](https://github.com/pyca/bcrypt/) 모듈을 예로 들 수 있다. 시스템 전체에 bcrypt가 설치되어 있지만 가상 환경에는 설치되어 있지 않다고 가정하여 본다.

이를 테스트하기 전에 `deactivate`를 실행하여 "시스템" 컨텍스트로 돌아가야 한다.
```shell
(env) $ deactivate
$
```

이것으로 셸 세션은 정상으로 돌아왔고 `python` 명령어는 글로벌 Python 설치를 참조한다. 특정 가상 환경을 사용한 후에는 항상 이 작업을 수행해야 한다.

이제 비밀번호를 해시하기 위해 사용하는 `bcrypt`를 설치한다.
```shell
$ pip -q install bcrypt
$ python -c "import bcrypt; print(bcrypt.hashpw('password'.encode('utf-8'), bcrypt.gensalt()))"
b'$2b$12$quzV3rSQ5a6qYnmeOs92SeuQhRgdMVaZVtW9ndbNV6FOA7GMR4kNK'
```

가상 환경을 활성화하여 동일한 명령을 시도하면 그 결과는 다음과 같다.
```shell
$ source env/bin/activate
(env) $ python -c "import bcrypt; print(bcrypt.hashpw('password'.encode('utf-8'), bcrypt.gensalt()))"
Traceback (most recent call last):
  File "<string>", line 1, in <module>
ModuleNotFoundError: No module named 'bcrypt'
```

위에서 보여주는 바와 같이 `source env/bin/activate` 수행 이후 `python -c "import bcrypt..."` 명령의 수행 결과가 바뀌었다.

"시스템" 컨텍스트에서는 bcrypt를 사용할 수 있지만 `env` 컨텍스트에서는 사용할 수 없다. 이것이 바로 쉘이 가상화 환경에서 실현하고 싶은 격리의 일종이다. 이처럼 쉽게 실현할 수 있다.

## Virtual Environments의 작동  <a id="how-does-a-virtual-environments-work"></a>
환경을 "activate"한다는 것은 정확히 어떤 의미일까? 특히 실행 환경, 의존관계 해결 등을 이해할 필요가 있는 경우 개발자에게는 어떤 일이 일어나고 있는 지를 파악하는 것이 매우 중요하다.

이것이 어떻게 작동하는지 설명하기 위해 먼저 다양한 python 실행 파일의 위치를 확인해 본다. 환경을 "deactivate"한 상태에서 다음을 실행한다.
```shell
$ which python
/usr/bin/python
```

이제 `env`를 활성화하고 다음 명령을 다시 실행한다.
```shell
$ source env/bin/activate
(env) $ which python
/home/yoonjoon.lee/Drills/Python/Venv_Test/env/bin/python
```

환경을 활성화한 후 활성 환경에서는 `$PATH` 환경 변수가 약간 수정되었기 때문에 `python` 실행 파일의 경로가 바뀌었다.

활성화 전후의 `$PATH`의 첫 번째 경로의 차이점에 주의해 살펴본다.
```shell
$ echo $PATH
/home/yoonjoon.lee/.local/bin:/home/yoonjoon.lee/.locakl/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

$ source env/bin/activate
(env) $ echo $PATH
/home/yoonjoon.lee/Drills/Python/Venv_Test/env/bin:/home/yoonjoon.lee/.local/bin:/home/yoonjoon.lee/.locakl/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```
후자의 예에서는 가상 환경의 bin 디렉토리가 경로의 먼저 나온다. 즉, 명령에서 실행 파일을 실행할 때 가장 먼저 검색되는 디렉토리인 것이다. 따라서 쉘은 시스템 전체 버전이 아닌 가상 환경의 Python 인스턴스를 사용한다.

> **Note**: [Anaconda](https://en.wikipedia.org/wiki/Anaconda_%28Python_distribution%29)처럼 Python을 번들하는 패키지 또한 실행힐 때 경로를 조작하는 경향이 있다. 다른 환경에서 문제가 발생할 경우를 대비해 이 점을 기억하여야 한다. 여러 환경을 동시에 활성화하기 시작하면 이러한 문제가 발생할 수 있다.

이 때문에 다음과 같은 의문이 생길 수 있을 것이다.

- 이 두 실행 파일의 차이점은 무엇인가?
- 가상 환경의 Python 실행 파일은 어떻게 시스템의 site-packages가 아닌 다른 것을 사용할 수 있을까?

Python의 시작 방법과 시스템에서의 위치로 위의 의문에 대한 답을 줄 수 있다. 위의 두 Python 실행 파일은 실제로 아무런 차이가 없다. 그러나 **중요한 것은 디렉토리 경로이다**.

Python은 시작할 때 Python 바이너리의 경로를 확인한다. 가상 환경에서 실제 시스템의 Python 바이너리의 복사 또는 심볼릭 링크에 불과하다. 그런 다음 이 경로에 따라 경로의 `bin` 부분을 생략하고 `sys.prefix`와 `sys.exec_prefix` 경로를 설정한다. 

그런 다음 상대 경로 `lib/pythonX.X/site-packages/`를 검색하여 `site-packages` 디렉토리를 찾기 위하여 `sys.prefix`에 있는 경로를 사용한다. 여기서 X.X는 사용 중인 Python 버전이다.

위의 예에서 Python 바이너리는 디렉토리 `/home/yoonjoon.lee/Drills/Python/Venv_Test/env/bin`에 있다. 즉, `sys.prefix`는 `/home/yoonjoon.lee/Drills/Python/Venv_Test/env`이므로 사용되는 `site-packages` 디렉토리는 `/home/yoonjoon.lee/Drills/Python/Venv_Test/env/lib/pythonX.X/site-packages`이다. 마지막으로 패키지를 저장하고 있는 모든 경로를 갖는 `sys.path array`에 이 경로를 저장한다.

## virtualenvwrapper로 Virtual Environments 관리 <a id="managing-virtual-environmants-with-virtualenv-wrapper"></a>
가상 환경은 패키지 관리의 몇 가지 큰 문제를 해결하지만 완벽하지 않다. 몇 가지 환경을 구축한 후, 환경 자체의 문제가 발생할 수 있다는 것을 알게 될 것이다. 개발자 스스로 환경을 관리하여 대부분 문제를 해결 할 수 있다. 이를 [virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/)가 지원한다. 이는 주 도구 `virtualenv` 주변의 래퍼 스크립트이다.

virtualenvwrapper는 다음과 같은 유용한 기능을 제공한다.

- 모든 가상 환경을 한 곳에 정리
- 환경을 쉽게 생성, 삭제 및 복사할 수 있는 기능 제공
- 환경간 전환을 위한 명령어 제공

이러한 기능 중 일부는 소소하거나 중요하지 않은 것처럼 보일 수 있지만, 개발 환경에 추가하여 중요한 도구로 사용될 수 있음을 곧 알게 될 것이다.

시작하려면 `pip`을 사용하여 래퍼를 다운로드한다.
```shell
$ pip install virtualenvwrapper
```

설치되면 셸 함수를 활성화해야 한다. 이를 하려면 `source virtualenvwrapper.sh`를 실행한다. pip을 사용하여 처음 설치하면 출력에 *virtualenvwrapper.sh*의 정확한 경로가 표시된다. 또는 다음을 실행하여 얻을 수 있다.

```shell
$ which virtualenvwrapper.sh
/usr/local/bin/virtualenvwrapper.sh
```

이 경로를 사용하여 셸의 시작 파일에 다음 세 줄을 추가한다. bash 쉘을 사용하는 경우 이러한 행을 `~/.bashrc` 또는 `~/.profile` 파일에 삽입한다. `zsh`, `csh` 또는 `fish` 등 다른 쉘의 경우 해당 쉘의 고유한 시작 파일에 적용해야 한다. 중요한 것은 로그인할 때 또는 새로운 쉘을 열 때 다음 명령어가 실행된다는 것이다.
```shell
export WORKON_HOME=$HOME/.virtualenvs   # Optional
export PROJECT_HOME=$HOME/projects      # Optional
source /usr/local/bin/virtualenvwrapper.sh
```

> **Note**: 환경 변수 `WORKON_HOME`과 `PROJECT_HOME`의 설정이 꼭 필요하지 않다. virtualenvwrapper에는 이 환경 변수의 기본값이 있지만 그 값을 설정하여 오버라이드할 수 있다.

마지막으로 스타트업 파일을 재실행한다.
```
$ source ~/.bashrc
```

이제 `$WORKON_HOME`는 virtualenvwrapper의 데이터와 파일이 있는 디렉토리를 가리킨다.
```shell
echo $WORKON_HOME
/home/yoonjoon.lee/.virtualenvs
```

이제 환경 관리에 도움을 주는 쉘 명령을 사용할 수 있다.  다음과 같이 사용 가능한 예를 둘 수 있다.

- [workon](http://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html#workon)
- [deactivate](http://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html#deactivate)
- [mkvirtualenv](http://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html#mkvirtualenv)
- [cdvirtualenv](https://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html#cdvirtualenv)
- [rmvirtualenv](https://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html#rmvirtualenv)

[이 문서](http://virtualenvwrapper.readthedocs.org/en/latest/install.html)에서 명령어, 설치 및 virtualenvwrapper 설정에 대한 보다 자세한 내용을 설명하고 있다.

이제 새로운 프로젝트를 시작할 때는 다음과 같이 해야 할 것이다.
```shell
$ mkvirtualenv my-new-project
(my-new-project) $
```

그러면 모든 virtualenvwrapper 환경이 저장되는 `$WORKON_HOME` 디렉토리에 새로운 환경을 생성하고 그 환경이 활성화된다.

이 환경의 사용을 중지하려면 다음과 같이 환경을 비활성화해야 한다.
```shell
(my-new-project) $ deactivate
$
```

선택할 수 있는 환경이 많을 경우 `workon` 기능을 사용하여 모든 환경을 나열할 수 있다.
```shell
$ workon
my-new-project
my-django-project
web-scraper
```

마지막으로 활성화는 다음과 같이 한다.
```shell
$ workon web-scraper
(web-scraper) $
```

단일 툴을 사용하여 Python 버전을 전환하고 싶다면 `virtualenv`를 사용할 수 있다. virtualenv에는 사용할 Python 버전을 선택할 수 있는 [파라미터](https://virtualenv.pypa.io/en/stable/reference/#cmdoption-p) `-p`를 제공한다. 그것을 `which` 명령과 조합하면 원하는 버전의 Python을 간단하게 선택할 수 있다. 예를 들어 Python 3 버전을 사용하고 싶다면 다음과 같이 할 수 있다.
```shell
$ virtualenv -p $(which python3) blog_virtualenv
```

그러면 새로운 Python 3 환경을 생성한다.

어떻게 작동할까? `$PATH` 변수에 지정된 경로를 검색하여 해당 명령어에 대한 절대 경로를 찾기 위해 `which` 명령을 사용한다. 따라서 `python3`에 대한 절대 경로가 `PYTHON_EXE`를 사용하는 `-p` 파라미터로 반환되었다. `python2`(또는 시스템의 기본 설정이 `python2`로 되어 있는 경우)를 `python3`으로 바꾼 경우 이를 `python2`에도 사용할 수 있다.

이제 환경을 어디에 설치했는지 기억할 필요가 없다. 원하는 대로 쉽게 삭제 또는 복사할 수 있으며 프로젝트 디렉토리가 간단하게 정리할 수 있다!

## 여러 버전의 Python 사용 <a id="using-different-versions-of-python"></a>

이전 `virtualenv` 툴과 달리 `pyvenv`는 Python의 임의 버전으로 환경 생성을 지원하지 않는다. 즉, 사용자가 생성하는 모든 환경에서 기본 Python 3 설치를 사용해야 합니다. 환경을 최신 시스템 버전인 Python으로 (--upgrade 옵션으로) 업그레이드할 수 있지만, 변경한 다음 특정 버전을 지정할 수 없다.

[Python 설치](https://realpython.com/installing-python/) 방법은 여러 가지가 있지만 바이너리의 다른 버전을 자주 제거하고 재설치할 수 있을 만큼 쉽고 유연한 방법은 거의 없다.

여기에 바로 [pyenv](https://github.com/yyuu/pyenv)의 역할이 있다.

`pyenv`와 `pyvenv`은 이름이 비슷하지만 `pyenv`는 프로젝트 레벨뿐만 아니라 시스템 레벨에서 Python 버전을 전환할 수 있도록 지원하는 데 중점을 두고 있다는 점에서 다르다. `pyvenv`는 모듈을 분리하지만 `pyenv`의 목적은 Python 버전을 분리하는 것이다.

[pyenv를 설치](https://jinmay.github.io/2019/03/16/linux/ubuntu-install-pyenv-1/)한다. 먼저 설치 준비가 필요하다. ubuntu를 포함한 여려가지 리눅스 배포판에서 패키지 설치를 위해 거치는 build 과정에 발생하는 문제를 방지하고자 필요한 패키지들을 설치한다.

```shell
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev
```

pyenv를 설치한다.
```shell
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
```

다음 쉘 시작파일 `.bashrc`에 다음 명령을 추가한다.
```shell
export PYENV_ROOT="$HOME/.pyenv
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```

추가한 다음 이를 반영하기 위하여 다음을 수행한다.
```shell
source ~/.bashrc
```

> **Note**: 아쉽게도 pyenv는 Windows를 지원하지 않는다. 시도해 볼 수 있는 몇 가지 대안으로 [pywin](https://github.com/davidmarble/pywin)과 [anyenv](https://realpython.com/python-virtual-environments-a-primer/#:~:text=are%20pywin%20and-,anyenv,-.)이 있다.

시스템에 pyenv를 설치하면 다음과 같은 기본적인 명령어를 사용할 수 있다.
```shell
$ pyenv install 3.5.0   # Install new version
$ pyenv versions        # List installed versions
$ pyenv exec python -V  # Execute 'python -V' using pyenv version
```

다음 예에서는 Python 3.5.0 버전을 설치하고 pyenv에 사용 가능한 모든 버전을 보여 달라고 요청한 다음 pyenv 지정 버전을 사용하여 python -V 명령을 실행하여 보자.

"golbal" 또는 "local"을 이용하여 더 많은 버전을 사용할 수 있다. `local` 명령어와 함께 `pyenv`를 사용하면 로컬 `.python-version` 파일에 버전를 저장하여 특정 프로젝트 또는 디렉토리의 Python 버전을 설정할 수 있다. "로컬" 버전은 다음과 같이 설정할 수 있다.

```shell
$ pyenv local 2.7.11
```

그러면 다음과 같이 현재 디렉토리에 `.python-version` 파일을 생성한다.
```shell
$ ls -la
total 16
drwxr-xr-x  4 yoonjoon.lee   yoonjoon.lee  136 Feb 22 10:57 .
drwxr-xr-x  9 yoonjoon.lee   yoonjoon.lee  306 Jan 27 20:55 ..
-rw-r--r--  1 yoonjoon.lee   yoonjoon.lee    7 Feb 22 10:57 .python-version
-rw-r--r--  1 yoonjoon.lee   yoonjoon.lee   52 Jan 28 17:20 main.py
```

이 파일은 "2.7.11" 내용만을 포함하고 있다. 이제 `pyenv`를 사용하여 스크립트를 실행하면 이 파일이 로드되고 시스템에서 실행 가능한 지정된 버전이 사용된다.

예를 들어 다음과 같은 `main.py`라는 간단한 스크립트가 프로젝트 디렉토리에 있다.
```python
import sys
print('Using version:', sys.version[:5])
```

사용되는 Python 실행 파일의 버전 번호만 출력한다. `pyenv`와 `exec` 명령어를 사용하면 설치한 Python의 다른 버전에서 스크립트를 실행할 수 있다.
```shell
$ python main.py
Using version: 2.7.5
$ pyenv global 3.5.0
$ pyenv exec python main.py
Using version: 3.5.0
$ pyenv local 2.7.11
$ pyenv exec python main.py
Using version: 2.7.11
```

`pyenv exec python main.py`은 기본적으로 "global" Python 버전을 사용하지만 현재 디렉토리의 버전으로 설정한 후에는 "local" 버전을 사용힌다.

버전 요건이 다양한 프로젝트를 수행하는 개발자에게 매우 강력한 기능일 것이다. ("global"을 통하여) 모든 프로젝트의 기본 버전을 쉽게 변경할 수 있을 뿐만 아니라 특별한 경우를 지정하여 오버라이드할 수도 있다.

## 맺으며 <a id="conclusion"></a>

여기에서 Python 의존관계를 저장하고 해결하는 방법뿐만 아니라 다양한 커뮤니티 툴을 이용하여 다양한 패키징 및 버전 관리 문제를 해결하는 방법에 대해 알아 보았다.

이처럼 거대한 Python 커뮤니티 덕분에 이러한 일반적인 문제를 해결할 수 있는 툴이 많이 있다. 개발자로 지내면서, 반드시 시간을 투자하여 이러한 툴의 이점을 활용하는 방법을 배워야 한다. 의도하지 않았지만 새로운 용도를 발견하거나 유사한 개념을 사용하는 다른 언어에 적용하는 방법을 배울 수도 있다.
